
public class App {

	public static void main(String[] args) {

		System.out.println("10_000 at 2% = " + calculateInterest(10000, 2.0));
		System.out.println("10_000 at 2% = " + calculateInterest(10000, 3.0));
		System.out.println("10_000 at 2% = " + calculateInterest(10000, 4.0));
		System.out.println("10_000 at 2% = " + calculateInterest(10000, 5.0));

		for (int i = 0; i < 4; i++) {

			System.out.println("Loop " + i + " hello.");

		}

		for (int i = 2; i < 9; i++) {

			System.out.println(" 10000 at " + i + " interest = " + calculateInterest(10000, i));

		}

		for (int i = 8; i > 1; i--) {

			System.out.println(" 10000 at " + i + " interest = " + calculateInterest(10000, i));

		}

		int count = 0;
		for (int i = 10; i < 50; i++) {

			if (isPrime(i) == true) {
				count++;
				System.out.println("Prime numbers: " + i);
				if (count == 3) {
					break;
				}
			}

		}

	}

	public static boolean isPrime(int n) {

		if (n == 1) {
			return false;
		}
		for (int i = 2; i <= n / 2; i++) {

			if (n % i == 0) {

				return false;
			}
		}
		return true;
	}

	public static double calculateInterest(double amount, double interestRate) {
		return (amount * interestRate / 100);
	}
}
